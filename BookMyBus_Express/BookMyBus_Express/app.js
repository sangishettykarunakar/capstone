var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var passportconfig = require('./routes/passport');
var session = require('express-session');

var TwitterStrategy = require('passport-twitter').Strategy;
//var cors = require('cors');

var index = require('./routes/index');
var users = require('./routes/users');
var registerAuthenticate = require('./routes/registerAuthenticate');
var adminModule = require('./routes/adminModule');
var userModule = require('./routes/userModule');
var twit = require('./routes/twit');

var app = express();



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//Mongose DB details
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/mydb');

var Schema = mongoose.Schema;

// Admin Login Schema
var adminLoginSchema = new mongoose.Schema({
  id: Number,
  userName: String,
  password: String
});

var adminLogin = mongoose.model('adminLogin', adminLoginSchema,'adminLogin');

app.use(function(req, res, next) {
  req.adminLogin = adminLogin;
  next();
});

// User Registartion Schema
var userRegisterSchema = new mongoose.Schema({
  id: Number,
  FullName: String,
  username: String,
  password: String,
  EmailID: String,
  MobileNumber : Number,
  
});

var userRegister = mongoose.model('userRegister', userRegisterSchema,'userRegister');

app.use(function(req, res, next) {
  req.userRegister = userRegister;
  next();
});

// Add bus Schema
var busDetailsSchema = new mongoose.Schema({
  id: Number,
  BusRouteNumber: String,
  BusDescription: String,
  FromCity: String,
  ToCity: String,
  TotalSeats : Number,  
});

var busDetails = mongoose.model('busDetails', busDetailsSchema,'busDetails');

app.use(function(req, res, next) {
  req.busDetails = busDetails;
  next();
});

// Add city Schema
var cityDetailsSchema = new mongoose.Schema({
  id: Number,
  CityName: String,
  CityDescription: String,  
});

var cityDetails = mongoose.model('cityDetails', cityDetailsSchema,'cityDetails');

app.use(function(req, res, next) {
  req.cityDetails = cityDetails;
  next();
});

// Booked Bus details Schema
var bookedBusSchema = new mongoose.Schema({
  BookingId:String,
  UserName:String,
  BusRouteNumber: String,
  BusDescription: String,
  FromCity: String,
  ToCity: String,
  TotalSeats : Number, 
  DateBooked  :String,
  BookedSeats : Number,
  AvailableSeats  :Number
});

var bookedBusDetails = mongoose.model('bookedBusDetails', bookedBusSchema,'bookedBusDetails');

app.use(function(req, res, next) {
  req.bookedBusDetails = bookedBusDetails;
  next();
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' ,cookie: { secure: true }}));
app.use(passport.initialize());
app.use(passport.session());
//app.use(cors());

app.use('/', index);
app.use('/users', users);
app.use('/registerAuthenticate', registerAuthenticate);
app.use('/adminModule', adminModule);
app.use('/userModule', userModule);
app.use('/twit', twit);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
